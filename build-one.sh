#!/usr/bin/env bash

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

IMAGE=""
ARCH=multi
DOCKER_REPO=dbogatov/docker-images

usage() {
	echo "Usage: $0 -i <IMAGE> [-a <ARCH=multi | linux/arm64/v8 | linux/amd64>]" 1>&2;
	exit 1;
}

while getopts "i:a:" o
do
	case "${o}" in
		i)
			IMAGE=${OPTARG}
			;;
		a)
			ARCH=${OPTARG}
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))

if [ -z $IMAGE ]
then
	usage
fi

arches=()

if [ $ARCH = "multi" ]
then
	arches+=( "linux/arm64/v8" "linux/amd64" )
else
	arches+=( $ARCH )
fi

echo "Will build for arch(es): ${arches[@]}"

for arch in ${arches[@]}
do
	echo "For ${arch}"

	tag_rach="${arch////$'-'}"

	set -x

	docker build \
		--no-cache \
		--pull \
		--platform $arch \
		-t ${IMAGE}_tmp-$tag_rach \
		images/${IMAGE}

	docker run -v ${CWD}/images/${IMAGE}/test.sh:/test.sh --entrypoint="/bin/bash" ${IMAGE}_tmp-$tag_rach /test.sh

	docker tag ${IMAGE}_tmp-$tag_rach $DOCKER_REPO:${IMAGE}-latest-$tag_rach
	docker push $DOCKER_REPO:${IMAGE}-latest-$tag_rach

	set +x
done

if [ $ARCH = "multi" ]
then
	set -x

	docker manifest rm $DOCKER_REPO:${IMAGE}-latest-multi-arch || true # it may exist, and we don't want just ammending to that
	docker manifest create \
		$DOCKER_REPO:${IMAGE}-latest-multi-arch \
		--amend $DOCKER_REPO:${IMAGE}-latest-"linux-amd64" \
		--amend $DOCKER_REPO:${IMAGE}-latest-"linux-arm64-v8"

	docker manifest push $DOCKER_REPO:${IMAGE}-latest-multi-arch

	set +x
fi

echo "Done!"
