# Docker Containers

This repo is a home for the images I use in my own build system.

## Structure

Each image has its folder.
In that folder there are:

* `README.md` which iterates what's inside the container.
* `test.sh` which contains simple "tests" for the container.
* `Dockerfile` used to build the container.
* Optionally some other files which get copied into the image

## CI

The generation of the images is automatic.
GitLab CI (as defined in `.gitlab-ci.yml`) is responsible for building,
testing and releasing (publishing) images to
[my repo](https://hub.docker.com/r/dbogatov/docker-containers/) in docker hub.

An image does not get released unless it passes its tests.

## Tests

Tests are ridiculously simple. Bash script verifies that each executable is in
place by asking for version (`blc --version`) or checking config validity
(`nginx -t`).

## Using CI

To (re)build a specific image, run a new pipeline with the `BUILD_IMAGE` variable.

## Images


### Alpine with Extras

In this image:

* Alpine (latest)
* curl (latest)
* bash (latest)
* unzip (latest)
* zip (latest)
* ca-certificates (latest)
* openssh (latest)
* sshpass (latest)
* build-base (latest)
	* make
	* gcc / g++
	* libc-dev
* poppler-utils (latest)
	* pdffonts
* jq (latest)


### Broken Links Checker

In this image:
* NodeJS v7
* Broken Link Checker (0.7.3)
* Bash
* http server (NPM)


### Broken Links Checker

In this image:
* NodeJS v7
* Broken Link Checker (0.7.8)
* Bash
* http server (NPM)


### Broken Links Inspector

In this image:

* NodeJS v14
* Broken Links Inspector (v1.3.2)
* Bash
* HTTP server (NPM)


### CSpell

In this image:

* NodeJS / NPM
* Bash
* CSpell (latest) + German dictionary
* Git


### Debian + Tools for packaging

In this image:
* Debuild
* Build essentials
* Lintian


### Doxygen

In this image:
* Doxygen (latest)
* bash (latest)
* git


### FAISS

In this image:
* gcc 8.2 (from source)
* FAISS (latest, from source)
* mkl (latest, from conda)
* google test and benchmark
* cmake 3.17.3
* rpclib (latest, from source)


### Gulp + Bower

In this image:
* NodeJS / NPM
* Gulp (latest)
* Bash
* Bower (latest)
* Git


### Jekyll + Dev dependencies

In this image:
* Ruby
* NodeJS v7
* Gulp (latest on NPM)
* Bower (latest on NPM)
* Bash


### K8S kubectl, gcloud and extra packages for setup manager

In this image:

* kubectl (1.12.1)
* skopeo (0.1.24)
* jq (latest)
* htpasswd (latest)
* curl (latest)
* python (latest)
* gcloud (267.0.0)


### Latex (tex-live)

In this image:

* Self-built Tex-live distribution from washington.edu
* wget
* unzip
* poppler-utils
* ARM64 arch does not have `biber` and `pdfjam`


### Latex Dev Container

In this image:

* Latex distribution (self-built)
* Fira Sans from [carrois/Fira](https://github.com/carrois/Fira)\
* git


### Latex with R and knitr

In this image:

* All from Latex image
* R and Rscript
* knitr


### Latex with Fira Sans font and stuff

In this image:

* Latex distribution (self-built)
* Fira Sans form [carrois/Fira](https://github.com/carrois/Fira)
* CMU fonts
* ghostscript
* NodeJS + NPM
* http-server
* imagemagick
* PAX and JRE
* python3-pygments
* ARM64 arch will install `biber` and `pdfjam`


### MkDocs + Material Theme with extensions

In this image:
* PIP
* Python
* MkDocs
* Material theme + extensions


### .NET Core SDK 1.1 + Dev dependencies

In this image:
* .NET Core SDK 1.1 with MSBuild (not `project.json`)
* NodeJS v8
* Doxygen (latest on apt-get repos)
* Gulp (latest on NPM)
* Bower (latest on NPM)
* Broken Link Checker (0.7.3)
* HTML Tidy 5.2.0
* Yarn


### .NET Core SDK + Dev dependencies

In this image:
* .NET Core SDK 2.0.3 with MSBuild (not `project.json`)
* NodeJS v8
* Doxygen (latest on apt-get repos)
* Gulp (latest on NPM)
* Bower (latest on NPM)
* Broken Link Checker (0.7.3)
* HTML Tidy 5.2.0
* Yarn


### NGINX + Alpine + PHP 7

In this image:
* NGINX (recent version)
* PHP 7 + FPM
* jq

This image also contains tools for generating PDF micro-website.
It assumes PDFs are in `/srv` directory.
Run `./build-index.sh` to generate the index page and config.
Example: `./build-index.sh "Physics" "https://git.dbogatov.org/frankfurt/physics" "834d520f289e116cafeded0933155a8c29830638"`


### Node

In this image:
* NodeJS v8
* Bash
* http-server


### Mini Debian with g++ and C++ libs

In this image:

* Minideb
* g++
* libpbc
* libgmp
* libssl (OpenSSL)
* libpthreads
* libgtest (from source)
* libhiredis (from source)
* libredis++ (from source)
* libaerospike (from source)
* librpc (from source)
* make
* cmake (from source)
* pip
* gcovr
* libgbenchmark (from source)
* libboost-all-dev


### PDF Link Checker

In this image:

* Alpine 3.9
* python 2.7
* pip
	* pdf-link-checker
* bli (v1.3.2)


### PHP 5

In this image:

* PHP 5


### Alpine with Extras

In this image:

* Alpine
* python 3
* gcc
* libxml2
* libressl
* curl
* WebP binaries
* pip
	* jinja2
	* css_html_js_minify
	* lesscpy
	* pyyaml
	* anybadge
	* markdown
	* scholarly
	* webptools
	* awscli
	* libsass
	* instagram-basic-display


### Mini Debian with g++ and C++ libs

In this image:

* librelic
* libgmp
* libssl (OpenSSL)
* libpthreads
* libgtest (from source)
* doxygen
* make
* cmake
* gdb
* pip
* gcovr
* libgbenchmark (from source)
* libboost-all-dev


### Text Metrics

In this image:

* python 3
* pandoc
* jq
* curl
* pip
	* textstat


### Tidy

In this image:
* NodeJS v7
* HTML Tidy 5.2.0
* Bash
* http-server (NPM)
