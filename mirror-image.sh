#!/usr/bin/env bash
set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

usage () {
    printf "usage: ./$0 <from> <to>\n"
    printf "where\n"
    printf "\t from - full path to source image\n"
    printf "\t to - tag of the image in mirror repo\n"

    exit 1;
}

if ! [ $# -eq 2 ] && ! [ $# -eq 3 ]
then
    usage
fi

FROM=$1
TO=$2

if [ -n "$3" ]
then
	echo "Multi ARCH"

	for arch in "linux/arm64/v8" "linux/amd64"
	do
		echo "For ${arch}"

		tag_rach="${arch////$'-'}"

		docker pull --platform $arch $FROM
		docker tag $FROM dbogatov/docker-sources:$TO-$tag_rach
		docker push dbogatov/docker-sources:$TO-$tag_rach
	done

	docker manifest create \
		dbogatov/docker-sources:$TO-multi-arch \
		--amend dbogatov/docker-sources:$TO-"linux-amd64" \
		--amend dbogatov/docker-sources:$TO-"linux-arm64-v8"

	docker manifest push dbogatov/docker-sources:$TO-multi-arch
else
	echo "Single ARCH"

	docker pull $FROM
	docker tag $FROM dbogatov/docker-sources:$TO
	docker push dbogatov/docker-sources:$TO

fi

echo "Done."
