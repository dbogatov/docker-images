# Latex with Fira Sans font and stuff

In this image:

* Latex distribution (self-built)
* Fira Sans form [carrois/Fira](https://github.com/carrois/Fira)
* CMU fonts
* ghostscript
* NodeJS + NPM
* http-server
* imagemagick
* PAX and JRE
* python3-pygments
* ARM64 arch will install `biber` and `pdfjam`
