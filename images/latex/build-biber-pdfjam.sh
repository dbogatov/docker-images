#!/usr/bin/env bash

set -e

ARCH=$(uname -a | awk '{print $(NF-1)}')

echo "Arch: $ARCH"

if [ "$ARCH" = "aarch64" ]
then

	cd /tmp
	apt update
	apt install -y \
		wget \
		perl \
		cpanminus \
		bash \
		git \
		curl \
		make \
		gcc \
		musl-dev \
		openssl \
		libssl-dev \
		libxslt-dev \
		libgcrypt-dev \
		unzip \
		gnupg \
		zlib1g \
		zlib1g-dev \
		libperl-dev \
		libxml2 \
		libxml2-dev \
		liblzma-dev \
		biber \
		zip

	rm -rf pdfjam
	git clone https://github.com/rrthomas/pdfjam.git
	cd pdfjam
	./build.sh
	./built_package/pdfjam-3.03/bin/pdfjam -h

	cp ./built_package/pdfjam-3.03/bin/pdfjam /pdfjam

	biber -h
	/pdfjam -h

	cp /usr/bin/biber /usr/local/texlive/2023/bin/aarch64-linux/
	cp /pdfjam /usr/local/texlive/2023/bin/aarch64-linux/pdfjam

	echo "Done."

else

	biber -h
	pdfjam -h

	echo "Done."

fi
