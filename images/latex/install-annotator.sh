#!/usr/bin/env bash

set -e

cd /tmp/
unzip latex-pax-master.zip
cd latex-pax-master/scripts/

apt-get update
apt-get install -y cpanminus default-jre
cpanm  File::Which
apt-get remove -y cpanminus
apt-get autoremove -y

cp ./pdfannotextractor.pl /
cd /

/pdfannotextractor.pl --version
/pdfannotextractor.pl /tmp/sample.pdf

rm -rf /tmp/*

echo "Done."
