#!/usr/bin/env bash

set -e

# Script to test the image

xelatex --version
pdflatex --version
biber --version
pdfinfo -v
pdfjam --version
ghostscript -v
nodejs -v
npm -v
http-server -v
[ -d /usr/share/fonts/truetype/cmu/ ]
convert -version
/pdfannotextractor.pl --version
python3 -c "import pygments"
