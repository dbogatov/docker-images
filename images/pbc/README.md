# Mini Debian with g++ and C++ libs

In this image:

* Minideb
* g++
* libpbc
* libgmp
* libssl (OpenSSL)
* libpthreads
* libgtest (from source)
* libhiredis (from source)
* libredis++ (from source)
* libaerospike (from source)
* librpc (from source)
* make
* cmake (from source)
* pip
* gcovr
* libgbenchmark (from source)
* libboost-all-dev
