#!/usr/bin/env bash

set -e

apt-get update
apt-get install -y --no-install-recommends libgmp3-dev libssl-dev libpthread-stubs0-dev make python-pip git libboost-all-dev

pip install gcovr setuptools

# PBC
dpkg -i libpbc.deb
mv /usr/lib/libpbc.so.1.0.0 /usr/lib/x86_64-linux-gnu/libpbc.so
cp /usr/lib/x86_64-linux-gnu/libpbc.so /usr/lib/x86_64-linux-gnu/libpbc.so.1

# CMake 3.17.3
cd /tmp/
version=3.17
build=3
mkdir ~/temp
cd ~/temp
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/
./bootstrap
make -j$(nproc)
make install
cd ~

# RPC lib
cd /tmp/
git clone https://github.com/rpclib/rpclib.git
cd rpclib
mkdir build
cd build
cmake ..
cmake --build .
make install
cd ~

# Aerospike
cd /tmp/
git clone https://github.com/aerospike/aerospike-client-c.git
cd aerospike-client-c
git submodule update --init
make
make install
cd ~

# Google Test
cd /tmp/ 
git clone https://github.com/google/googletest.git
cd googletest
cmake CMakeLists.txt
make
make install
cd ~

# Google becnhmark
cd /tmp/ 
git clone https://github.com/google/benchmark.git
git clone https://github.com/google/googletest.git benchmark/googletest
cd benchmark
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make
make install
cd ~

# HI-Redis
cd /tmp/ 
git clone https://github.com/redis/hiredis.git
cd hiredis
make
make install
cd ~

# Redis++
cd /tmp/ 
git clone https://github.com/sewenew/redis-plus-plus.git
cd redis-plus-plus
mkdir compile
cd compile
cmake -DCMAKE_BUILD_TYPE=Release -DREDIS_PLUS_PLUS_CXX_STANDARD=17 ..
make
make install
cd ~

# remove temporary files
apt-get purge -y git
apt-get autoremove -y
rm -rf /usr/lib/libpbc.so* /libpbc* /var/lib/apt/lists/* /usr/src/* /tmp/*
