#!/usr/bin/env bash

set -e

# Script to test the image

g++ -v
make -v
cmake --version
pip -V
gcovr --version
[ -f /usr/lib/x86_64-linux-gnu/libgmp.so ]
[ -f /usr/lib/x86_64-linux-gnu/libpbc.so ]
[ -f /usr/lib/x86_64-linux-gnu/libpthread.so ]
[ -f /usr/lib/x86_64-linux-gnu/libssl.so ]
[ -f /usr/lib/x86_64-linux-gnu/libboost_atomic.so ]
[ -f /usr/local/lib/libgtest.a ]
[ -f /usr/local/lib/libbenchmark.a ]
[ -f /usr/local/lib/libhiredis.so ]
[ -f /usr/local/lib/libredis++.so ]
[ -f /usr/lib/libaerospike.so ]
[ -f /usr/local/lib/librpc.a ]
