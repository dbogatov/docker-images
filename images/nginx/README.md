# NGINX + Alpine + PHP 7

In this image:
* NGINX (recent version)
* PHP 7 + FPM
* jq

This image also contains tools for generating PDF micro-website.
It assumes PDFs are in `/srv` directory.
Run `./build-index.sh` to generate the index page and config.
Example: `./build-index.sh "Physics" "https://git.dbogatov.org/frankfurt/physics" "834d520f289e116cafeded0933155a8c29830638"`
