#!/usr/binenv bash

set -e

# Script to test the image

nginx -t
[ -d "/pdf" ]
jq -h
