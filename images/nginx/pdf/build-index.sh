#!/bin/bash

set -e

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

usage () {
	printf "usage: $0 <project-name> <project-url> <commit-sha>\n"

	exit 1;
}

if ! [ $# -eq 3 ]
then
	usage
fi

PROJECTNAME=$1
PROJECTURL=$2
COMMIT=$3

cp ./index-pdf.html /srv/index.html
rm -f pdfs.html

echo "Scanning /srv for PDFs"

PDFs=(/srv/*.pdf)

echo "Found ${#PDFs[@]} PDfs"

echo "Generating index..."

for pdf in ${PDFs[*]}
do

base=$(basename $pdf)
noext=${base%.pdf}

cat >> pdfs.html << EOT
	<div class="col-md-4">
		<h2>${noext}</h2>
		<p><a class="btn btn-secondary" href="./${base}" role="button">View PDF &raquo;</a></p>
	</div>
EOT

done

sed -i '/__FILES__/{
    s/__FILES__//g
    r pdfs.html
}' /srv/index.html

SHORT=$(echo $COMMIT | cut -c1-8)
COMMITURL="<a href='$PROJECTURL/commit/$COMMIT'>$SHORT</a>"

sed -i -e "s#__PRJECT__#$PROJECTNAME#g" /srv/index.html
sed -i -e "s#__COMMIT__#$COMMITURL#g" /srv/index.html

cp ./nginx.conf /etc/nginx/http.d/default.conf

echo "Cleaning up"

rm -f pdfs.html

echo "Done!"
