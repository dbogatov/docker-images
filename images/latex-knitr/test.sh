#!/usr/bin/env bash

set -e

# Script to test the image

Rscript --version
Rscript -e "library(knitr); knit('/tmp/main.rtex')"
