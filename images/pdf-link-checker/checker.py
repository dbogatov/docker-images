#!/usr/bin/env python

import sys
import re
import logging as log
import urlparse
from HTMLParser import HTMLParser
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdftypes import PDFStream

ESC_PAT = re.compile(r'[\000-\037&<>()"\042\047\134\177-\377]')

def e(s):
	return ESC_PAT.sub(lambda m: '&#%d;' % ord(m.group(0)), s)


def is_valid(url):
	protocol = urlparse.urlparse(url)[0]
	return protocol


def search_url_string(obj):
	if isinstance(obj, str):
		return e(obj)


def search_url(obj, urls):
	if obj is None:
		return

	if isinstance(obj, dict):
		for (k, v) in obj.iteritems():
			if k == 'URI':
				url = search_url_string(v)
				parser = HTMLParser()
				url = parser.unescape(url)
				if url is not None and is_valid(url):
					log.debug('URL found: {}'.format(url))
					urls.add(url)

			search_url(v, urls)

	elif isinstance(obj, list):
		for v in obj:
			search_url(v, urls)

	elif isinstance(obj, PDFStream):
		search_url(obj.attrs, urls)


def extract_urls(filename, urls):

	log.info('Checking links in file {} ...'.format(filename))

	try:
		fp = file(filename, 'rb')
		parser = PDFParser(fp)
		doc = PDFDocument(parser)
	except Exception as e:
		log.error("Cannot open file {}: {}\n".format(filename, e))
		return

	for xref in doc.xrefs:
		for objid in xref.get_objids():
			try:
				obj = doc.getobj(objid)
				search_url(obj, urls)
			except:
				continue

	fp.close()
	return


def main():
	urls = set()
	for file in sys.argv[1:]:
		extract_urls(file, urls)

	for url in urls:
		print(url)


if __name__ == "__main__":
	main()
