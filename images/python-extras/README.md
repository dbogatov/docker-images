# Alpine with Extras

In this image:

* Alpine
* python 3
* gcc
* libxml2
* libressl
* curl
* WebP binaries
* pip
	* jinja2
	* css_html_js_minify
	* lesscpy
	* pyyaml
	* anybadge
	* markdown
	* scholarly
	* webptools
	* awscli
	* libsass
	* instagram-basic-display
