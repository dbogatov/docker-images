#!/usr/bin/env bash

set -e

# Script to test the image

python3 --version
python3 -c "import jinja2"
python3 -c "import css_html_js_minify"
python3 -c "import lesscpy"
python3 -c "import yaml"
python3 -c "import markdown"
python3 -c "import scholarly"
python3 -c "import webptools"
python3 -c "import sass"
python3 -c "import instagram_basic_display"
anybadge -h
aws --version
curl --help
/usr/local/lib/python3.8/site-packages/lib/libwebp_linux/bin/cwebp -h
/usr/local/lib/python3.8/site-packages/lib/libwebp_linux/bin/gif2webp -h
