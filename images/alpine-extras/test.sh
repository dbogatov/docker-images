#!/usr/bin/env bash

set -e

# Script to test the image

unzip -hh
curl --version
bash --version
which ssh
which scp
sshpass -V
make -v
gcc -v
pdffonts -v
jq --help
zip -v
unzip -v
