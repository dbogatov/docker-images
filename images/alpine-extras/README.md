# Alpine with Extras

In this image:

* Alpine (latest)
* curl (latest)
* bash (latest)
* unzip (latest)
* zip (latest)
* ca-certificates (latest)
* openssh (latest)
* sshpass (latest)
* build-base (latest)
	* make
	* gcc / g++
	* libc-dev
* poppler-utils (latest)
	* pdffonts
* jq (latest)
