#!/usr/bin/env bash

set -e

apt-get update
apt-get install -y --no-install-recommends build-essential libgmp-dev libssl-dev libpthread-stubs0-dev cmake python3-pip git libboost-all-dev gdb doxygen

# Relic (no idea why but we need cmake from latest master but code from released v6...)
cd /tmp/
git clone https://github.com/relic-toolkit/relic.git
cd relic
git checkout 6b90545
mkdir relic-sym
# For symmetric
cd relic-sym
cmake -DLABEL=sym ..
../preset/gmp-pbc-ss1536.sh ../
git checkout 0.6.0
cmake -DLABEL=sym ..
../preset/gmp-pbc-ss1536.sh ../
make
make install
cd ..
# For asymmetric
mkdir relic-asym
cd relic-asym
git checkout 6b90545
cmake -DLABEL=asym ..
../preset/gmp-pbc-bn254.sh ../
git checkout 0.6.0
cmake -DLABEL=asym ..
../preset/gmp-pbc-bn254.sh ../
make
make install
cd ~

# GCovr
pip install --break-system-packages gcovr setuptools

# Google Test
cd /tmp/
git clone https://github.com/google/googletest.git
cd googletest
cmake CMakeLists.txt
make
make install
cd ~

# Google becnhmark
cd /tmp/
git clone https://github.com/google/benchmark.git
git clone https://github.com/google/googletest.git benchmark/googletest
cd benchmark
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make
make install
cd ~

# remove temporary files
apt-get autoremove -y
rm -rf /var/lib/apt/lists/* /usr/src/* /tmp/*
