# Mini Debian with g++ and C++ libs

In this image:

* librelic
* libgmp
* libssl (OpenSSL)
* libpthreads
* libgtest (from source)
* doxygen
* make
* cmake
* gdb
* pip
* gcovr
* libgbenchmark (from source)
* libboost-all-dev
