#!/usr/bin/env bash

set -e

# Script to test the image

g++ -v
make -v
cmake --version
pip -V
gcovr --version
doxygen -v

[ -f /usr/local/lib/libgtest.a ]
[ -f /usr/local/lib/libbenchmark.a ]
[ -f /usr/local/lib/librelic_asym.so ]
[ -f /usr/local/lib/librelic_sym.so ]

ARCH=$(uname -a | awk '{print $(NF-1)}')

echo "Arch: $ARCH"

if [ "$ARCH" = "aarch64" ]
then
	[ -f /usr/lib/aarch64-linux-gnu/libgmp.so ]
	[ -f /usr/lib/aarch64-linux-gnu/libpthread.a ]
	[ -f /usr/lib/aarch64-linux-gnu/libssl.so ]
	[ -f /usr/lib/aarch64-linux-gnu/libboost_atomic.so ]
else
	[ -f /usr/lib/x86_64-linux-gnu/libgmp.so ]
	[ -f /usr/lib/x86_64-linux-gnu/libpthread.a ]
	[ -f /usr/lib/x86_64-linux-gnu/libssl.so ]
	[ -f /usr/lib/x86_64-linux-gnu/libboost_atomic.so ]
fi
