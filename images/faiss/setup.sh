#!/usr/bin/env bash

set -e

conda install -y -c pytorch mkl

# utilities
apt-get update
apt-get install --no-install-recommends -y make wget libssl-dev libpthread-stubs0-dev git libboost-all-dev bzip2 zip unzip build-essential

pip install gcovr setuptools

# GCC 8.2
cd /tmp/
wget http://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-8.2.0/gcc-8.2.0.tar.gz
tar zxf gcc-8.2.0.tar.gz
cd gcc-8.2.0
./contrib/download_prerequisites
./configure --disable-multilib
make -j$(nproc)
make install
cd ~

# CMake 3.17.3
cd /tmp/
version=3.17
build=3
mkdir ~/temp
cd ~/temp
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/
./bootstrap
make -j$(nproc)
make install
cd ~

# Google Test
cd /tmp/
git clone https://github.com/google/googletest.git
cd googletest
cmake CMakeLists.txt
make -j$(nproc)
make install
cd ~

# Google becnhmark
cd /tmp/
git clone https://github.com/google/benchmark.git
git clone https://github.com/google/googletest.git benchmark/googletest
cd benchmark
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make -j$(nproc)
make install
cd ~

# faiss
cd /tmp/
git clone https://github.com/facebookresearch/faiss.git
cd faiss
cmake -DFAISS_ENABLE_GPU=OFF -DFAISS_ENABLE_PYTHON=OFF -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=ON -B build .
make -C build faiss
make -C build install
cd ~

# RPC lib
cd /tmp/
git clone https://github.com/rpclib/rpclib.git
cd rpclib
mkdir build
cd build
cmake ..
cmake --build .
make install
cd ~

# remove temporary files
apt-get purge -y git
apt-get autoremove -y
rm -rf /var/lib/apt/lists/* /tmp/*
