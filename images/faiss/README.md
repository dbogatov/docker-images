# FAISS

In this image:
* gcc 8.2 (from source)
* FAISS (latest, from source)
* mkl (latest, from conda)
* google test and benchmark
* cmake 3.17.3
* rpclib (latest, from source)
