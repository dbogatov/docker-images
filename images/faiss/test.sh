#!/bin/bash
set -e

# Script to test the image

gcc --version
g++ --version
[ -f /usr/local/lib/libfaiss.so ]
[ -f /usr/local/lib/librpc.a ]
