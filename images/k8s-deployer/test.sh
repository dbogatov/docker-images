#!/usr/bin/env bash

set -e

# Script to test the image

skopeo -v
kubectl version --client=true
jq --version
htpasswd -b -n user password
curl --version 
gcloud --version
