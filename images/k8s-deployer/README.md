# K8S kubectl, gcloud and extra packages for setup manager

In this image:

* kubectl (1.12.1)
* skopeo (0.1.24)
* jq (latest)
* htpasswd (latest)
* curl (latest)
* python (latest)
* gcloud (267.0.0)
