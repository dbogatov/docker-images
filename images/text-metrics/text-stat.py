#!/usr/bin/env python3

import textstat
import sys

with open(sys.argv[1]) as input:
	text = input.read()

	print(textstat.text_standard(text, float_output=True))
