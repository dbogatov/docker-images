#!/usr/bin/env bash

set -e

# Script to test the image

pandoc --version
python3 -c "import textstat"
jq -h
curl --version
