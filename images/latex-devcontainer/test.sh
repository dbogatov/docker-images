#!/usr/bin/env bash

set -e

# Script to test the image

xelatex --version
pdflatex --version
biber --version
pdfjam --version
git --version
pdfinfo -v
